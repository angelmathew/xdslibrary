import React from 'react';
import PropTypes from 'prop-types';
 
import { HeaderStyled, HeaderTitleStyled } from './styles';
 
const Header = props => {
  return (
    <HeaderStyled light = {props.light}>
      <HeaderTitleStyled>
        Gatsby Project + Storybook
      </HeaderTitleStyled>
    </HeaderStyled>
  )
};
 
Header.propTypes = {
  light: PropTypes.bool,
};
 
export default Header;