import React from 'react';

import Layout from '../global/Layout';
import Header from '../components/Header';

const IndexPage = () => (
  <Layout>
    <Header/>
  </Layout>
);
 
export default IndexPage;